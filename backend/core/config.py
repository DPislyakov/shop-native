import pathlib

import os

from pydantic import AnyHttpUrl, BaseSettings, EmailStr, validator
from typing import List, Optional, Union

from schemas.logger import LogConfig
import logging


ROOT = pathlib.Path(__file__).resolve().parent.parent


class Settings(BaseSettings):

    PROJECT_NAME: str = "ShopNative"
    PROJECT_VERSION: str = "0.0.1"

    API_V1_STR: str = "/api/v1"
    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = []

    SQLALCHEMY_DATABASE_URI: Optional[str] = "sqlite:///./dev.db"
    DATABASE_DOCKER_PG = "postgresql://postgres:postgres@postgres:5432/postgres"
    FIRST_SUPERUSER: EmailStr = "admin@shop.com"

    SENDLK_TOKEN = "1011|WFCl1Rqj7BojQMuf97BMp20hoE1eU69NfJzpxbID"
    SENDER_ID = "SendTest"

    DATABASE_DEV_STAND = "postgresql://postgres:postgres@45.131.41.227:5432/postgres"

    # SECRET_KEY: str = os.getenv("SECRET_KEY")
    SECRET_KEY: str = b"""-----BEGIN RSA PRIVATE KEY-----
    MIIEpAIBAAKCAQEA0iffju92MRGFz5EYx2qeBBSSQbw4kbtJdZUROdKviD+gN1rf
    J9d70J+QrJIm/gTXIqBiIrQ3spTELODwEua4lGDFOQi7mLV05Ug8hvJimoWgwQoH
    0uUUlI0eZjdhRHlquK/29Hg8vfm8+Cbk4t4mUuxH8K0/m+/VmXE48y3x2se7rLSB
    qoPq+XY145gAm5RFd63CVzXCYhE3aQ/BxhytImcF6l2wJrGkOPPo/zbJsGWYTGco
    TEQLeJyZ/1NDW0qEeD0878SEj4lO1tsZ/N72kpf4xqdx+I3m6IZPBazDlZut1avZ
    vQDFkn+sH8pNkK43iq4j8ecUI6xBQ7nrYo2TlQIDAQABAoIBABAte5swOeP7Glbp
    L4QiY/mib8PZrm0hgr0ndYUC//o4E6hPOYCCqQhdUDDIq3HAg6Mpv4Q2ZVc89oER
    68k4s5rQfD4wuug9h7sivW4R+J82jDJwCDwm4FezKVTrDHTN9/YxrYwSOXLSZ8ae
    j94bBc46XCtd2tixNLs0KRMnRUf3Fl+RYbNFpQC9ueeU5eCcm+/UmLCmHTKil7y4
    QT+7bPV/BphL7OQWZnmkJogB7wTT2iW9mbECw7d8sZ7Np+G/XJfiJF1zWB2hhwWz
    SlnFUClovvDiSF/a6m+0lTqcZ/kwPb72uJ9QHcfIZAFgroGJsAZh/O+JYUluHJlj
    NaYrnWECgYEA+bBlXW8xUYsQlY0VRS2YtZCsMtxsK5N9FtBHgFk5lKnFF9FnfMNH
    +ZtlKDF6LbLVkv7BIkidHu+ImJnuFxX4vYcD3gIAvUhGVXOOCcKB5Mck4xJecGRu
    2wytpTEvTSFj+CIECPLhPhM161psxY8Uk78TkqSHWe0CT6b5pLqszg0CgYEA13et
    tqTM7XFNl4sxNxCxGAY8pmc14hJu3OOuBdp1Wn4DGVB/zpyI8lBLaHOX0bBjeCf1
    hC6OyLKqtSeGbP+v8H0WCnY6cDRAQGq+ktaRiqDubIt56NwPgQtXUnCyTr4Wx9uE
    HQEXZ9aozlTIJhx7+igorvXgXuKc2bzUn0j5gakCgYB0D5Fed/QFwk1oJpcve71T
    EDdqwNiq/LiVZSUHPPIt7ygFJTokXSVUme3QXAc9vLXXyQD/LZ+TJKZqwIpSQs1H
    U73xcR7k3PaMe9UXJkl4jTTRM9WgCp8YxPDmK1GgWZf9W9piSnt2fjl+9549yn83
    GgdAD6g2nZuIaMX70yXF9QKBgQCx84empqkRGViJ5k15OvHn8463p9oaojqpd+jL
    sMbnO9d3WsKTczivV51P4NfsOI+ONwJQbttB6j2Oa9xbvTgjmEtRzJ8q+BxwlzhZ
    sZh2M792KiM/LDAp1aIC1kyNN/U8jpGoGrSn7+NutRqwTyodSnl/NlZeRy5frdK0
    0uIuMQKBgQDl+riu24/NWKIkr1CfMDtRmKRNqFmjVuFLBIIovyUd6wNi5gwI4h46
    b+h859SED2NyBBhrAl65FiT3amyjD1IY71TjGWEKt31Y0L0/FxLvvimmM5PAHuql
    W10Chx8GaEXhD1lEFZ8uwZm/1xfJQqXiPzp/jsKbLDNn6ZYmyEFNfw==
    -----END RSA PRIVATE KEY-----"""

    ALGORITHM = "HS256"
    ACCESS_TOKEN_EXPIRE_MINUTES = 30

    class Config:
        env_file = './.env'
        case_sensitive = True


logging.config.dictConfig(LogConfig().dict())
logger = logging.getLogger("basic")

settings = Settings()
