from sqlalchemy import Table
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import ForeignKey

from db.base_class import Base

CategoriesProductRelation = Table("categoriesproductrelation", Base.metadata,
                                  Column("categories_id", Integer, ForeignKey("category.id")),
                                  Column("products_id", Integer, ForeignKey("product.id"))
                                  )
