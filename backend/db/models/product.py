from sqlalchemy import Column
from sqlalchemy import Integer, Float
from sqlalchemy import String, Text
from sqlalchemy.orm import relationship
from sqlalchemy import ARRAY
from sqlalchemy import DateTime
from sqlalchemy import Boolean
from sqlalchemy import Table
from sqlalchemy import ForeignKey
from sqlalchemy import JSON

from datetime import datetime

from db.base_class import Base
from db.models.category_rel_products import CategoriesProductRelation
from db.models.group_rel_products import GroupsProductRelation


PromoProductRelation = Table("promosproductrelation", Base.metadata,
                             Column("products_id", Integer, ForeignKey("product.id")),
                             Column("promos_id", Integer, ForeignKey("promo.id"))
                             )


ModifierProductRelation = Table("modifiersproductrelation", Base.metadata,
                                Column("products_id", Integer, ForeignKey("product.id")),
                                Column("modifier_id", Integer, ForeignKey("modifier.id"))
                                )


class Product(Base):

    id = Column(Integer, primary_key=True, index=True)
    date = Column(DateTime, default=datetime.utcnow)
    name = Column(String(256), unique=False, nullable=True)
    description = Column(String(256), unique=False, nullable=True)
    price = Column(Float, unique=False, nullable=True)
    old_price = Column(Float, unique=False, nullable=True)
    quantity = Column(Integer, unique=False, nullable=True)
    weight = Column(Integer, unique=False, nullable=True)
    length = Column(Integer, unique=False, nullable=True)
    width = Column(Integer, unique=False, nullable=True)
    height = Column(Integer, unique=False, nullable=True)
    categories = relationship("Category",
                              secondary=CategoriesProductRelation,
                              back_populates="products",
                              cascade="all, delete")
    images = Column(ARRAY(String))
    type = Column(String, unique=False, nullable=True)
    unit = Column(String, unique=False, nullable=True)
    groups = relationship("Group",
                          secondary=GroupsProductRelation,
                          back_populates="products",
                          cascade="all, delete")
    is_favorite = Column(Boolean, unique=False, default=False)
    promos = relationship("Promo",
                          secondary=PromoProductRelation,
                          back_populates="products",
                          cascade="all, delete")
    modifiers = relationship("Modifier",
                          secondary=ModifierProductRelation,
                          back_populates="products",
                          cascade="all, delete")

    def to_json(self):
        return {
                "id": self.id,
                "name": self.name,
                "description": self.description,
                "price": self.price,
                "old_price": self.old_price,
                "is_favorite": self.is_favorite,
                "characteristic": None,
                "semi_characteristic": None,
                "product_preview": None,
                "quantity": self.quantity,
                "weight": self.weight,
                "length": self.length,
                "width": self.width,
                "height": self.height,
                "categories": [category.to_dict() for category in self.categories],
                "variants": [],
                "images": self.images,
                "params": {
                    "id": self.id,
                    "type": self.type,
                    "name": self.name,
                    "unit": self.unit,
                    "values": []
                },
                "modifiers": [modifier.to_dict() for modifier in self.modifiers]
            }


class Promo(Base):
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, unique=False, nullable=False)
    description = Column(String(256), unique=False, nullable=True)
    image = Column(String, unique=False, nullable=False)
    url = Column(String, unique=False, nullable=True)
    type = Column(String, unique=False, nullable=True)

    products = relationship("Product",
                            secondary=PromoProductRelation,
                            back_populates="promos",
                            cascade="all, delete")

    category_id = Column(Integer, ForeignKey("category.id"))
    categories = relationship("Category", back_populates="promos")


class ModifierType(Base):
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=False, nullable=True)
    modifiers = relationship("Modifier", back_populates="modifiertypes")


class Modifier(Base):
    id = Column(Integer, primary_key=True, index=True)

    type_id = Column(Integer, ForeignKey("modifiertype.id"))
    modifiertypes = relationship("ModifierType", back_populates="modifiers")

    products = relationship("Product",
                            secondary=ModifierProductRelation,
                            back_populates="modifiers",
                            cascade="all, delete")

    name = Column(String, unique=False, nullable=True)
    price = Column(Float, unique=False, nullable=True)
    max_count = Column(Integer, unique=False, nullable=True)
    default = Column(Boolean, unique=False, nullable=True)
    image = Column(String, unique=False, nullable=True)


class Characteristic(Base):
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(526), unique=False, nullable=True)
    subtitle = Column(Text, unique=False, nullable=True)
    is_sub = Column(Boolean, unique=False, default=False)
    product = Column(Integer, ForeignKey("product.id"), on_delete='CASCADE')


class Cart(Base):
    id = Column(Integer, primary_key=True, index=True)
    owner_id = Column(Integer, ForeignKey("user.id"), on_delete='CASCADE')
    product = Column(Integer, ForeignKey("product.id"), on_delete='CASCADE')
    amount = Column(Integer, unique=False, nullable=False)


class Delivery(Base):
    id = Column(Integer, primary_key=True, index=True)
    owner_id = Column(Integer, ForeignKey("user.id"), on_delete='CASCADE')
    offer_json = Column(JSON, nullable=False, unique=False)


class CartSetting(Base):
    id = Column(Integer, primary_key=True, index=True)
    date = Column(DateTime, default=datetime.utcnow)
    settings_json = Column(JSON, nullable=False, unique=False)
