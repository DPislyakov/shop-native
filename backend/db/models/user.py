from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Date

from db.base_class import Base


class User(Base):
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=False, nullable=True)
    phone = Column(String, unique=True, nullable=False)
    email = Column(String, unique=False, nullable=True)
    sex = Column(Boolean(), nullable=True, unique=False)
    birthday = Column(Date, nullable=True)
    is_active = Column(Boolean(), default=True)
    is_superuser = Column(Boolean(), default=False)
