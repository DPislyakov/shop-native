from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Boolean
from sqlalchemy.orm import relationship

from db.base_class import Base
from db.models.category_rel_products import CategoriesProductRelation


class Category(Base):
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, unique=False, nullable=False)
    image = Column(String, unique=False, nullable=True)

    products = relationship("Product",
                            secondary=CategoriesProductRelation,
                            back_populates="categories",
                            cascade="all, delete")

    promos = relationship("Promo", back_populates="categories")
    is_home = Column(Boolean, unique=False, default=False)