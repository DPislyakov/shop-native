from sqlalchemy import Table
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import ForeignKey

from db.base_class import Base

GroupsProductRelation = Table("groupsproductrelation", Base.metadata,
                                  Column("groups_id", Integer, ForeignKey("group.id")),
                                  Column("products_id", Integer, ForeignKey("product.id"))
                                  )