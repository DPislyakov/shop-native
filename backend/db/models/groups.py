from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship

from db.base_class import Base
from db.models.group_rel_products import GroupsProductRelation


class Group(Base):
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, unique=True, nullable=False)

    products = relationship("Product",
                            secondary=GroupsProductRelation,
                            back_populates="groups",
                            cascade="all, delete")
