from sqlalchemy.orm import Session

from core.hashing import Hasher
from db.models.user import User
from schemas.users import UserCreate, UpdateUser


def create_new_user(user: UserCreate, db: Session):
    user = User(
        phone=user.phone,
        # token=token,
        is_active=True,
        is_superuser=False,
    )
    db.add(user)
    db.commit()
    db.refresh(user)
    return user


def update_user(id: int, user: UpdateUser, db: Session):
    existing_user = db.query(User).filter(User.id == id)
    if not existing_user.first():
        return 0
    user.__dict__.update()
    existing_user.update(user.__dict__)
    db.commit()
    return 1


def get_user(phone: str, db: Session):
    user = db.query(User).filter(User.phone == phone).first()
    return user
