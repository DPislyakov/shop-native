from sqlalchemy.orm import Session

from db.models.category import Category
from schemas.category import CategoryCreate


def create_category(category: CategoryCreate, db: Session):
    category = Category(
        title=category.title,
        image=f"https://shopmobile-five.vercel.app/v1/app/home/category/image/{category.image}",
        is_home=category.is_home
    )
    db.add(category)
    db.commit()
    db.refresh(category)
    return category


def update_category(id: int, category: CategoryCreate, db: Session):
    existing_user = db.query(Category).filter(Category.id == id)
    if not existing_user.first():
        return 0
    category.__dict__.update()
    existing_user.update(category.__dict__)
    db.commit()
    return 1


def get_cur_category(id: str, db: Session):
    category = db.query(Category).filter(Category.id == id).first()
    return category


def get_all_category(db: Session):
    return db.query(Category).order_by(Category.title).all()


def get_home_category(db: Session):
    return db.query(Category).filter(Category.is_home == 'True').all()
