from sqlalchemy.orm import Session

from db.models.product import Promo
from schemas.promo import PromoCreate


def create_promo(promo: PromoCreate, db: Session):
    promo = Promo(
        title=promo.title,
        description=promo.description,
        image=promo.image,
        url=promo.url,
        type=promo.type,
        category_id=promo.category_id
    )
    db.add(promo)
    db.commit()
    db.refresh(promo)
    return promo


def update_promo(id: int, promo: PromoCreate, db: Session):
    existing_promo = db.query(Promo).filter(Promo.id == id)
    if not existing_promo.first():
        return 0
    promo.__dict__.update()
    existing_promo.update(promo.__dict__)
    db.commit()
    return 1


def get_promo(id: str, db: Session):
    promo = db.query(Promo).filter(Promo.id == id).first()
    return promo


def get_all_promo(db: Session):
    return db.query(Promo).order_by(Promo.title).all()
