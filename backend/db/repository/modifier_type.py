from sqlalchemy.orm import Session

from db.models.product import ModifierType
from schemas.modifier_type import ModifierTypeCreate


def create_modifier_type(modifier_type: ModifierTypeCreate, db: Session):
    modifier_type = ModifierType(
        name=modifier_type.name
    )
    db.add(modifier_type)
    db.commit()
    db.refresh(modifier_type)
    return modifier_type


def update_modifier_type(id: int, modifier_type: ModifierTypeCreate, db: Session):
    existing_modifier_type = db.query(ModifierType).filter(ModifierType.id == id)
    if not existing_modifier_type.first():
        return 0
    modifier_type.__dict__.update()
    existing_modifier_type.update(modifier_type.__dict__)
    db.commit()
    return 1


def get_modifier_type(id: str, db: Session):
    modifier_type = db.query(ModifierType).filter(ModifierType.id == id).first()
    return modifier_type


def get_all_modifier_type(db: Session):
    return db.query(ModifierType).order_by(ModifierType.name).all()
