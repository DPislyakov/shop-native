from sqlalchemy.orm import Session

from db.models.groups import Group
from schemas.groups import GroupCreate


def create_group(group: GroupCreate, db: Session):
    group = Group(
        title=group.title
    )
    db.add(group)
    db.commit()
    db.refresh(group)
    return group


def update_group(id: int, group: GroupCreate, db: Session):
    existing_group = db.query(Group).filter(Group.id == id)
    if not existing_group.first():
        return 0
    group.__dict__.update()
    existing_group.update(group.__dict__)
    db.commit()
    return 1


def get_group(id: str, db: Session):
    group = db.query(Group).filter(Group.id == id).first()
    return group


def get_all_groups(db: Session):
    return db.query(Group).order_by(Group.title).all()
