from sqlalchemy.orm import Session
import functools
import operator

from db.models.product import Product, Modifier
from db.models.category import Category
from db.models.groups import Group
from db.models.product import Promo, PromoProductRelation
from schemas.product import ProductCreate

from typing import List

from core.config import logger


def create_product(product: ProductCreate, db: Session):
    product = Product(
        name=product.name,
        description=product.description,
        price=product.price,
        old_price=product.old_price,
        quantity=product.quantity,
        weight=product.weight,
        length=product.length,
        width=product.width,
        height=product.height,
        categories=convert_to_category(items=product.categories, db=db),
        images=product.images,
        type=product.type,
        unit=product.unit,
        groups=convert_to_group(items=product.groups, db=db),
        is_favorite=product.is_favorite,
        promos=convert_to_promo(items=product.promos, db=db) if product.promos else [],
        modifiers=convert_to_modifiers(items=product.modifiers, db=db)
    )
    db.add(product)
    db.commit()
    db.refresh(product)
    return product


def update_product(id: int, product: ProductCreate, db: Session):
    existing_product = db.query(Product).filter(Product.id == id)
    if not existing_product.first():
        return 0
    product.__dict__.update()
    existing_product.update(product.__dict__)
    db.commit()
    return 1


def product_add_promo(id: int, promos: int, db: Session):
    ins = PromoProductRelation.insert().values(products_id=id, promos_id=promos)
    db.execute(ins)
    db.commit()
    return 1


def get_product(id: str, db: Session):
    product = db.query(Product).filter(Product.id == id).first()
    return product


def get_list_products(offset: int, limit: int, category_ids: List[int], sort: List, search_name: str, db: Session):
    if len(category_ids) == 0:
        return db.query(Product).filter(Product.name.ilike(f"%{search_name}%")).order_by(*sort).offset(offset).\
            limit(limit).all()
    return db.query(Product).join(Category, Product.categories).filter(Category.id.in_(category_ids)). \
        filter(Product.name.ilike(f"%{search_name}%")).offset(offset).limit(limit).all()
        # filter(Product.name.contains(search_name)).offset(offset).limit(limit).all()


def get_recommends_products(product: Product, db: Session):
    name_list = [word for word in product.name.split(" ") if len(word) > 1]
    return functools.reduce(operator.concat, [db.query(Product).filter(Product.name.ilike(f"%{current_name}%"))
                            .filter(Product.id != product.id)
                            .limit(10).all() for current_name in name_list])


def convert_to_category(items: list, db: Session):
    result = list()
    for item in items:
        category = db.query(Category).filter(Category.id == item).first()
        if category:
            result.append(category)
    return result


def convert_to_group(items: list, db: Session):
    result = list()
    for item in items:
        group = db.query(Group).filter(Group.id == item).first()
        if group:
            result.append(group)
    return result


def convert_to_promo(items: list, db: Session):
    result = list()
    for item in items:
        promo = db.query(Promo).filter(Promo.id == item).first()
        if promo:
            result.append(promo)
    return result


def convert_to_modifiers(items: list, db: Session):
    result = list()
    for item in items:
        modifier = db.query(Modifier).filter(Modifier.id == item).first()
        if modifier:
            result.append(modifier)
    return result

