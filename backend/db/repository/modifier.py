from sqlalchemy.orm import Session

from db.models.product import Modifier
from schemas.modifier import ModifierCreate


def create_modifier(modifier: ModifierCreate, db: Session):
    modifier = Modifier(
        name=modifier.name,
        price=modifier.price,
        max_count=modifier.max_count,
        default=modifier.default,
        type_id=modifier.type_id,
        image=modifier.image
    )
    db.add(modifier)
    db.commit()
    db.refresh(modifier)
    return modifier


def update_modifier(id: int, modifier: ModifierCreate, db: Session):
    existing_modifier = db.query(Modifier).filter(Modifier.id == id)
    if not existing_modifier.first():
        return 0
    modifier.__dict__.update()
    existing_modifier.update(modifier.__dict__)
    db.commit()
    return 1


def get_modifier(id: str, db: Session):
    modifier = db.query(Modifier).filter(Modifier.id == id).first()
    return modifier


def get_all_modifier(db: Session):
    return db.query(Modifier).order_by(Modifier.name).all()
