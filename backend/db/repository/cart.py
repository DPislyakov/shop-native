from sqlalchemy.orm import Session

from db.models.product import Cart, Delivery, CartSetting
from schemas.product import ProductAddToCart, CartOffer
from schemas.cart_settings import CartSettings

from core.config import logger


def add_cart_record(product: ProductAddToCart, owner_id: int, db: Session):
    existing_cart = db.query(Cart).filter(Cart.owner_id == owner_id).filter(Cart.product == product.product_id)
    if not existing_cart.first():
        cart = Cart(
            owner_id=owner_id,
            product=product.product_id,
            amount=product.amount
        )
        db.add(cart)
        db.commit()
        db.refresh(cart)
        return cart
    else:
        existing_cart.update({"amount": product.amount})
        db.commit()
        return existing_cart.first()


def get_cart(owner_id: int, db: Session):
    cart = db.query(Cart).filter(Cart.owner_id == owner_id).all()
    return cart


def update_cart(owner_id: int, product_id: int, amount: int, db: Session):
    existing_cart = db.query(Cart).filter(Cart.owner_id == owner_id).filter(Cart.product == product_id)
    if not existing_cart.first():
        return 0
    existing_cart.update({"amount": amount})
    db.commit()
    return 1


def delete_product_from_cart(owner_id: int, product_id: int, db: Session):
    db.query(Cart).filter(Cart.owner_id == owner_id).filter(Cart.product == product_id).delete()
    db.commit()
    return


def place_an_order_db(offer: CartOffer, owner_id: int, db: Session):
    delivery = Delivery(owner_id=owner_id, offer_json=offer.dict())
    db.add(delivery)
    db.commit()
    db.refresh(delivery)
    return delivery


def delete_all_product_from_cart(owner_id: int, db: Session):
    db.query(Cart).filter(Cart.owner_id == owner_id).delete()
    db.commit()
    return


def pull_cart_settings(settings: CartSettings, db: Session):
    cart_settings = CartSetting(settings_json=settings.dict())
    db.add(cart_settings)
    db.commit()
    db.refresh(cart_settings)
    return cart_settings


def get_last_cart_settings(db: Session):
    result = db.query(CartSetting).all()
    return [setting.settings_json for setting in result]
    #return db.query(CartSetting).order_by(CartSetting.id.desc()).first()
