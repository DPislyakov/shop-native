from sqlalchemy.orm import Session

from db.models.product import Characteristic
from schemas.characteristic import CharacteristicCreate


def create_characteristic(characteristic: CharacteristicCreate, db: Session):
    characteristic = Characteristic(
        title=characteristic.title,
        subtitle=characteristic.subtitle,
        is_sub=characteristic.is_sub,
        product=characteristic.product
    )
    db.add(characteristic)
    db.commit()
    db.refresh(characteristic)
    return characteristic


def update_characteristic(id: int, group: CharacteristicCreate, db: Session):
    existing_group = db.query(Characteristic).filter(Characteristic.id == id)
    if not existing_group.first():
        return 0
    group.__dict__.update()
    existing_group.update(group.__dict__)
    db.commit()
    return 1


def get_characteristic(id: str, db: Session):
    group = db.query(Characteristic).filter(Characteristic.id == id).first()
    return group


def get_characteristic_by_product_id(id: str, db: Session):
    group = db.query(Characteristic).filter(Characteristic.product == id).filter(Characteristic.is_sub == False)\
        .all()
    return group


def get_semi_characteristic_by_product_id(id: str, db: Session):
    group = db.query(Characteristic).filter(Characteristic.product == id).filter(Characteristic.is_sub == True)\
        .all()
    return group


def get_all_characteristics(db: Session):
    return db.query(Characteristic).order_by(Characteristic.title).all()
