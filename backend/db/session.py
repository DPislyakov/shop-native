from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from core.config import settings

from typing import Generator


# engine = create_engine(
#     settings.SQLALCHEMY_DATABASE_URI,
#     connect_args={"check_same_thread": False},
# )

engine = create_engine(
    settings.DATABASE_DOCKER_PG
)

# engine = create_engine(
#     settings.DATABASE_DEV_STAND
# )

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def get_db() -> Generator:
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()
