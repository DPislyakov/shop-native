from fastapi import APIRouter
from fastapi import Depends
from fastapi import Response
from fastapi.responses import JSONResponse
from fastapi import HTTPException
from fastapi import status
from sqlalchemy.orm import Session
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer

from db.repository.users import get_user, create_new_user
from schemas.users import UserCreate
from db.session import get_db
from schemas.tokens import TokenVerifyIn, TokenVerifyOut, TokenRefresh
from core.auth import Auth

import logging


router = APIRouter()
security = HTTPBearer()
auth_handler = Auth()


@router.post('/phone/init', summary="Инициализация аккаунта")
async def init(data: UserCreate, db: Session = Depends(get_db)):
    try:
        user = get_user(phone=data.phone, db=db)
        if not user:
            create_new_user(user=data, db=db)
        return Response(status_code=201)
    except:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка определения номера телефона"
        )


@router.post('/phone/verify', summary="Верификация кода доступа", response_model=TokenVerifyOut)
async def verify(response: Response, data: TokenVerifyIn, db: Session = Depends(get_db)):
    try:
        user = get_user(phone=data.phone, db=db)
        if not user:
            raise HTTPException(status_code=401, detail='Invalid phone')
        access_token = auth_handler.encode_token(data.phone)
        refresh_token = auth_handler.encode_refresh_token(data.phone)
        response.set_cookie(
            key="access_token", value=f"Bearer {refresh_token}", httponly=True
        )
        json_resp = {'access_token': access_token, 'refresh_token': refresh_token}
        return JSONResponse(json_resp, status_code=200)
    except:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка авторизации"
            )


@router.post('/secret', include_in_schema=False)
def secret_data(credentials: HTTPAuthorizationCredentials = Depends(security)):
    token = credentials.credentials
    if auth_handler.decode_token(token):
        return 'Top Secret data only authorized users can access this info'
    return Response(status_code=404)


@router.post('/refresh', summary="Обновление токена", response_model=TokenVerifyOut)
def refresh_token(token: TokenRefresh,
                  credentials: HTTPAuthorizationCredentials = Depends(security)):
    try:
        new_token = auth_handler.refresh_token(token.refresh_token)
        json_res = {
            "access_token": new_token,
            "refresh_token": token.refresh_token
        }
        return JSONResponse(
            content=json_res,
            status_code=status.HTTP_201_CREATED
        )
    except:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Неверный токен"
        )
