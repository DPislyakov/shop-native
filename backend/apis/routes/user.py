from fastapi import APIRouter
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi import Depends
from fastapi.responses import JSONResponse, Response
from fastapi import status
from fastapi import HTTPException

from sqlalchemy.orm import Session

from db.session import get_db
from core.auth import Auth
from schemas.users import ShowUser, UpdateUser
from db.repository.users import get_user, update_user


router = APIRouter()
security = HTTPBearer()
auth_handler = Auth()


@router.get("/", summary="Получение информации о пользователе", response_model=ShowUser)
async def get_user_info(credentials: HTTPAuthorizationCredentials = Depends(security),
                        db: Session = Depends(get_db)):
    try:
        token = credentials.credentials
        user_phone = auth_handler.decode_token(token=token)
        current_user = get_user(phone=user_phone, db=db)
        json_res = {
            "name": current_user.name,
            "phone": current_user.phone,
            "email": current_user.email,
            "sex": current_user.sex,
            "birthday": current_user.birthday.isoformat() if current_user.birthday else None
        }
        return JSONResponse(json_res, status_code=status.HTTP_200_OK)
    except:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Пользователь не найден"
        )


@router.put("/", summary="Изменение информации пользователя")
async def update_info_about_user(user: UpdateUser,
                                 credentials: HTTPAuthorizationCredentials = Depends(security),
                                 db: Session = Depends(get_db)):
    try:
        token = credentials.credentials
        user_phone = auth_handler.decode_token(token=token)

        current_user = get_user(phone=user_phone, db=db)
        update_user(id=current_user.id, user=user, db=db)
        return Response(status_code=status.HTTP_201_CREATED)
    except:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Пользователь не найден"
        )


