from fastapi import APIRouter
from fastapi.security import HTTPBearer
from fastapi import Depends, File, UploadFile
from fastapi.responses import JSONResponse, Response, FileResponse
from fastapi import status
from fastapi import HTTPException
from fastapi import Query

from sqlalchemy.orm import Session

from typing import List
from enum import Enum

from db.session import get_db
from db.repository.product import (get_list_products, create_product, get_product,
                                   get_recommends_products, product_add_promo)
from db.repository.characteristic import (create_characteristic, get_characteristic_by_product_id,
                                          get_semi_characteristic_by_product_id)
from db.models.product import Product
from db.repository.modifier import create_modifier, get_all_modifier
from db.repository.modifier_type import create_modifier_type, get_all_modifier_type

from schemas.product import ProductCreate, ProductPreview
from schemas.characteristic import CharacteristicCreate
from schemas.promo import PromoAdd
from schemas.modifier import ModifierCreate
from schemas.modifier_type import ModifierTypeCreate

from apis.utils import sort_enum

from core.config import logger


router = APIRouter()
security = HTTPBearer()


class SortFilter(Enum):
    priceUp = 1
    priceDown = 2
    nameUp = 3
    nameDown = 4
    dateUp = 5
    dateDown = 6

# @router.get("/category", summary="Получение всех возможных категорий")
# async def get_category(db: Session = Depends(get_db)):
#     try:
#         list_category = get_all_category(db=db)
#         result = list()
#         for category in list_category:
#             result.append(
#                 {
#                     "id": category.id,
#                     "title": category.title,
#                     "image": category.image
#                 }
#             )
#         json_res = {
#             "result": result
#         }
#         return JSONResponse(json_res, status_code=status.HTTP_200_OK)
#     except:
#         raise HTTPException(
#             status_code=status.HTTP_404_NOT_FOUND,
#             detail="Ошибка получения категорий"
#         )


@router.post("/", summary="Создание продукта")
async def create_single_product(product: ProductCreate,
                                db: Session = Depends(get_db)):
    try:
        product = create_product(product=product, db=db)
        return JSONResponse(content={"name": product.name}, status_code=status.HTTP_201_CREATED)
    except:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка создания продукта"
        )


@router.get("/", summary="Получение списка продуктов")
async def get_all_products(offset: int = Query(0, ge=0),
                           limit: int = Query(100, le=100),
                           category_filter: List[int] = Query([]),
                           sort: List[str] = Query([]),
                           name: str = Query(""),
                           db: Session = Depends(get_db)):
    try:
        sorted_list = sort_enum(sort_enum=SortFilter, sort_list=sort)
        list_products = get_list_products(offset=offset,
                                          limit=limit,
                                          category_ids=category_filter,
                                          sort=sorted_list,
                                          search_name=name,
                                          db=db)
        result = [ProductPreview(**product.to_dict()).dict() for product in list_products]
        return JSONResponse(content={"result": result},
                            status_code=status.HTTP_200_OK)
    except HTTPException:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка получения списка продуктов"
        )


@router.get("/{id}", summary="Получение списка продуктов")
async def get_current_product(id: int,
                              db: Session = Depends(get_db)):
    try:
        product = get_product(id=id, db=db)
        result = product.to_json()
        result.update({"characteristic": [char.to_dict() for char in
                                          get_characteristic_by_product_id(id=product.id, db=db)]})
        result.update({"semi_characteristic": [char.to_dict() for char in
                                               get_semi_characteristic_by_product_id(id=product.id, db=db)]})
        result.update({"product_preview": await get_recommend_products(current_product=product, db=db)})
        return JSONResponse(content=result, status_code=status.HTTP_200_OK)
    except HTTPException:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка получения продукта"
        )


async def get_recommend_products(current_product: Product, db: Session):
    return [ProductPreview(**product.to_dict()).dict() for product in
            get_recommends_products(product=current_product, db=db)]


@router.post("/characteristic", summary="Добавление характеристики для продукта")
async def add(characteristic: CharacteristicCreate, db: Session = Depends(get_db)):
    try:
        characteristic = create_characteristic(characteristic=characteristic, db=db)
        if characteristic:
            return JSONResponse(content={"title": characteristic.title}, status_code=status.HTTP_201_CREATED)
        else:
            raise HTTPException(status_code=404)
    except HTTPException:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка добавления характеристики для продукта"
        )


@router.post("/{id}/promo", summary="Добавление промо к продукту")
async def add_promo(id: int,
                    promo: PromoAdd,
                    db: Session = Depends(get_db)):
    res = product_add_promo(id=id, promos=promo.promo, db=db)
    if res:
        return JSONResponse(content={"result": "success"}, status_code=status.HTTP_201_CREATED)
    else:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка добавления промо к продукту"
        )



# @router.get("/category/image/{category_image}", summary="Получить изображение категории", include_in_schema=False)
# async def get_image_category(category_image: str):
#     try:
#         file_dir = os.listdir("/var/task/media/category")
#         if category_image in file_dir:
#             return FileResponse(path=f"/var/task/media/category/{category_image}",
#                                 media_type="application/octet-stream",
#                                 filename=category_image)
#         else:
#             raise HTTPException(
#                 status_code=status.HTTP_404_NOT_FOUND,
#                 detail="Изображение не найдено"
#             )
#     except:
#         raise HTTPException(
#             status_code=status.HTTP_400_BAD_REQUEST,
#             detail="Ошибка получения изображения категории"
#         )
#
#
# @router.post("/category/image/upload", summary="Загрузить изображение категории", include_in_schema=False)
# async def upload_image_category(file: UploadFile = File(...)):
#     try:
#         contents = await file.read()
#         with open(f"/var/task/media/category/{file.filename}", "wb") as f:
#             f.write(contents)
#         return JSONResponse({"filename": file.filename},
#                             status_code=status.HTTP_201_CREATED)
#     except:
#         raise HTTPException(
#             status_code=status.HTTP_400_BAD_REQUEST,
#             detail="Ошибка загрузки изображения категории"
#         )
