import os

from fastapi import APIRouter
from fastapi.security import HTTPBearer
from fastapi import Depends, File, UploadFile
from fastapi.responses import JSONResponse, Response, FileResponse
from fastapi import status
from fastapi import HTTPException

from sqlalchemy.orm import Session

from schemas.category import CategoryCreate
from db.session import get_db
from db.repository.category import get_all_category, create_category, get_cur_category, get_home_category


router = APIRouter()


@router.get("/", summary="Получение всех возможных категорий")
async def get_category(db: Session = Depends(get_db)):
    try:
        list_category = get_all_category(db=db)
        result = list()
        for category in list_category:
            result.append(
                {
                    "id": category.id,
                    "title": category.title,
                    "image": category.image
                }
            )
        json_res = {
            "result": result
        }
        print(get_home_category(db=db))
        return JSONResponse(json_res, status_code=status.HTTP_200_OK)
    except:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Ошибка получения категорий"
        )


@router.get("/{id}", summary="Получение текущей категории")
async def get_current_category(id: int, db: Session = Depends(get_db)):
    try:
        category = get_cur_category(id=id, db=db)
        json_res = {
            "id": category.id,
            "title": category.title,
            "image": category.image
        }
        return JSONResponse(json_res, status_code=status.HTTP_200_OK)
    except:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Ошибка получения категории"
        )


@router.post("/", summary="Создание категории")
async def create_single_category(category: CategoryCreate,
                          db: Session = Depends(get_db)):
    try:
        create_category(category=category, db=db)
        return Response(status_code=status.HTTP_201_CREATED)
    except:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка создания категории"
        )


@router.get("/image/{category_image}", summary="Получить изображение категории", include_in_schema=False)
async def get_image_category(category_image: str):
    try:
        file_dir = os.listdir("/var/task/media/category")
        if category_image in file_dir:
            return FileResponse(path=f"/var/task/media/category/{category_image}",
                                media_type="application/octet-stream",
                                filename=category_image)
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Изображение не найдено"
            )
    except:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка получения изображения категории"
        )


@router.post("/image/upload", summary="Загрузить изображение категории", include_in_schema=False)
async def upload_image_category(file: UploadFile = File(...)):
    try:
        contents = await file.read()
        with open(f"/var/task/media/category/{file.filename}", "wb") as f:
            f.write(contents)
        return JSONResponse({"filename": file.filename},
                            status_code=status.HTTP_201_CREATED)
    except:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка загрузки изображения категории"
        )