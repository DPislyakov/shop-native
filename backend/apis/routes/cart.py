import os
from typing import List

from fastapi import APIRouter
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi import Depends, File, UploadFile
from fastapi.responses import JSONResponse, Response, FileResponse
from fastapi import status
from fastapi.encoders import jsonable_encoder
from fastapi import HTTPException

from sqlalchemy.orm import Session

from schemas.product import (ProductAddToCart, CartOut, ProductPreview,
                             CartOfferIn, CartOffer)
from schemas.cart_settings import CartSettings
from db.session import get_db
from db.models.product import Cart
from db.repository.users import get_user
from db.repository.product import get_product
from db.repository.cart import (add_cart_record, get_cart, delete_product_from_cart,
                                delete_all_product_from_cart, update_cart, place_an_order_db,
                                pull_cart_settings, get_last_cart_settings)
from core.auth import Auth

from core.config import logger


router = APIRouter()
security = HTTPBearer()
auth_handler = Auth()


async def convert_cart_out(carts: List[Cart], db: Session):
    result = list()
    for cart in carts:
        product = get_product(id=cart.product, db=db)
        result.append(
            jsonable_encoder(CartOut(
                product=ProductPreview(**product.to_dict()).dict(),
                amount=cart.amount)
            )
        )
    return result


@router.get("/", summary="Получить содержимое корзины")
async def get_cart_product(credentials: HTTPAuthorizationCredentials = Depends(security),
                           db: Session = Depends(get_db)):
    user_phone = auth_handler.decode_token(token=credentials.credentials)
    current_user = get_user(phone=user_phone, db=db)
    result = await convert_cart_out(carts=get_cart(owner_id=current_user.id, db=db), db=db)
    logger.info(result)
    return JSONResponse({"result": result}, status_code=status.HTTP_200_OK)


@router.post("/add", summary="Добавить продукт в корзину")
async def add(item: ProductAddToCart,
              credentials: HTTPAuthorizationCredentials = Depends(security),
              db: Session = Depends(get_db)):
    token = credentials.credentials
    user_phone = auth_handler.decode_token(token=token)
    current_user = get_user(phone=user_phone, db=db)
    add_cart_record(product=item, owner_id=current_user.id, db=db)
    return JSONResponse({"result": "Product added"}, status_code=status.HTTP_201_CREATED)


@router.delete("/", summary="Удалить содержимое корзины")
async def delete_cart_product(credentials: HTTPAuthorizationCredentials = Depends(security),
                              db: Session = Depends(get_db)):
    user_phone = auth_handler.decode_token(token=credentials.credentials)
    current_user = get_user(phone=user_phone, db=db)
    delete_all_product_from_cart(owner_id=current_user.id, db=db)
    return JSONResponse({"result": "Cart is clear"}, status_code=status.HTTP_200_OK)


@router.delete("/{product_id}", summary="Удалить конкретный продукт из корзины")
async def delete_current_cart_product(product_id: int,
                                      credentials: HTTPAuthorizationCredentials = Depends(security),
                                      db: Session = Depends(get_db)):
    user_phone = auth_handler.decode_token(token=credentials.credentials)
    current_user = get_user(phone=user_phone, db=db)
    delete_product_from_cart(owner_id=current_user.id, product_id=product_id, db=db)
    return JSONResponse({"result": f"Product with id {product_id} delete"}, status_code=status.HTTP_200_OK)


@router.put("/edit", summary="Изменить продукт в корзине")
async def edit_product_in_cart(item: ProductAddToCart,
                               credentials: HTTPAuthorizationCredentials = Depends(security),
                               db: Session = Depends(get_db)):
    token = credentials.credentials
    user_phone = auth_handler.decode_token(token=token)
    current_user = get_user(phone=user_phone, db=db)
    update_cart(owner_id=current_user.id,
                product_id=item.product_id,
                amount=item.amount,
                db=db)
    return JSONResponse({"result": "Product edited"}, status_code=status.HTTP_201_CREATED)


@router.post("/", summary="Оформить заказ")
async def place_an_order(offer: CartOfferIn,
                         credentials: HTTPAuthorizationCredentials = Depends(security),
                         db: Session = Depends(get_db)):
    token = credentials.credentials
    user_phone = auth_handler.decode_token(token=token)
    current_user = get_user(phone=user_phone, db=db)
    ready_offer = CartOffer(
        username=current_user.name,
        phone=current_user.phone,
        delivery=offer.delivery,
        payment_status=offer.payment_status,
        payment_amount=offer.payment_amount
    )
    place_an_order_db(offer=ready_offer, owner_id=current_user.id, db=db)
    return JSONResponse({"result": "Order placed"}, status_code=status.HTTP_201_CREATED)


@router.post("/settings", summary="Установить настройку корзины")
async def set_cart_settings(settings: CartSettings,
                            db: Session = Depends(get_db)):
    pull_cart_settings(settings=settings, db=db)
    return JSONResponse({"result": "Cart settings pulled"}, status_code=status.HTTP_201_CREATED)


@router.get("/settings", summary="Получить актуальную настройку корзины")
async def get_cart_settings(db: Session = Depends(get_db)):
    settings = get_last_cart_settings(db=db)
    return JSONResponse({"result": settings}, status_code=status.HTTP_200_OK)
