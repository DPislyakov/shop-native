import os

from fastapi import APIRouter
from fastapi.security import HTTPBearer
from fastapi import Depends, File, UploadFile
from fastapi.responses import JSONResponse, Response, FileResponse
from fastapi import status
from fastapi import HTTPException

from sqlalchemy.orm import Session

from schemas.category import CategoryCreate
from schemas.groups import GroupCreate
from schemas.promo import PromoCreate

from db.session import get_db
from db.repository.category import get_all_category, create_category, get_cur_category, get_home_category
from db.repository.groups import create_group, get_all_groups
from db.repository.promo import create_promo, get_all_promo, get_promo

import logging


router = APIRouter()
security = HTTPBearer()


@router.get("/category", summary="Получение всех возможных категорий")
async def get_category(db: Session = Depends(get_db)):
    try:
        list_category = get_home_category(db=db)
        result = list()
        for category in list_category:
            result.append(
                {
                    "id": category.id,
                    "title": category.title,
                    "image": category.image
                }
            )
        json_res = {
            "result": result
        }
        return JSONResponse(json_res, status_code=status.HTTP_200_OK)
    except:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Ошибка получения категорий"
        )


@router.get("/category/{id}", summary="Получение текущей категории")
async def get_current_category(id: int, db: Session = Depends(get_db)):
    try:
        category = get_cur_category(id=id, db=db)
        if category.is_home:
            json_res = {
                "id": category.id,
                "title": category.title,
                "image": category.image
            }
            return JSONResponse(json_res, status_code=status.HTTP_200_OK)
    except:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Ошибка получения категории"
        )


@router.get("/groups", summary="Получение всех возможных групп товаров")
async def get_groups(db: Session = Depends(get_db)):
    try:
        list_groups = get_all_groups(db=db)
        result = list()
        for group in list_groups:
            products = group.products
            result_products = list()
            for product in products:
                result_products.append(
                    {
                        "id": product.id,
                        "name": product.name,
                        "price": product.price,
                        "old_price": product.old_price,
                        "images": product.images,
                        "is_favorite": product.is_favorite
                    }
                )
            result.append(
                {
                    "id": group.id,
                    "title": group.title,
                    "products": result_products
                }
            )
        json_res = {
            "result": result
        }
        return JSONResponse(json_res, status_code=status.HTTP_200_OK)
    except:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Ошибка получения группы"
        )


@router.post("/group", summary="Создание группы")
async def create_single_group(group: GroupCreate,
                              db: Session = Depends(get_db)):
    try:
        create_group(group=group, db=db)
        return Response(status_code=status.HTTP_201_CREATED)
    except:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка создания группы"
        )


@router.post("/promo", summary="Создание промо")
async def create_single_promo(promo: PromoCreate,
                              db: Session = Depends(get_db)):
    try:
        create_promo(promo=promo, db=db)
        return Response(status_code=status.HTTP_201_CREATED)
    except:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка создания промо"
        )


@router.get("/promo", summary="Получение всех возможных промо")
async def get_promos(db: Session = Depends(get_db)):
    try:
        list_promos = get_all_promo(db=db)
        result = list()
        for promo in list_promos:
            result.append(
                {
                    "id": promo.id,
                    "image": promo.image,
                    "promo_type": promo.type
                }
            )
        json_res = {
            "result": result
        }
        return JSONResponse(json_res, status_code=status.HTTP_200_OK)
    except:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Ошибка получения промо"
        )


@router.get("/promo/{id}", summary="Получение промо по ID")
async def get_current_promo(id: int, db: Session = Depends(get_db)):
    try:
        promo = get_promo(id=id, db=db)

        result_products = list()
        for product in promo.products:
            result_products.append(
                {
                    "id": product.id,
                    "name": product.name,
                    "price": product.price,
                    "old_price": product.old_price,
                    "images": product.images,
                    "is_favorite": product.is_favorite
                }
            )

        json_res = {
            "result": {
                "id": promo.id,
                "title": promo.title,
                "description": promo.description,
                "image": promo.image,
                "products": result_products,
                "category_id": promo.category_id,
                "url": promo.url
            }
        }
        return JSONResponse(json_res, status_code=status.HTTP_200_OK)
    except:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Ошибка получения текущего промо"
        )