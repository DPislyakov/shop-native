from fastapi import APIRouter
from fastapi import Depends
from fastapi.responses import JSONResponse
from fastapi import status
from fastapi import HTTPException

from sqlalchemy.orm import Session

from db.session import get_db
from db.repository.modifier import create_modifier, get_all_modifier
from db.repository.modifier_type import create_modifier_type, get_all_modifier_type
from schemas.modifier import ModifierCreate
from schemas.modifier_type import ModifierTypeCreate


from core.config import logger


router = APIRouter()


@router.post("/", summary="Создание модификации")
async def create_single_modifier(modifier: ModifierCreate,
                                 db: Session = Depends(get_db)):
    try:
        modifier = create_modifier(modifier=modifier, db=db)
        return JSONResponse(content={"name": modifier.name}, status_code=status.HTTP_201_CREATED)
    except:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка создания продукта"
        )


@router.get("/", summary="Получение списка модификаций")
async def get_modifiers(db: Session = Depends(get_db)):
    try:
        modifiers = get_all_modifier(db=db)
        result = list()
        for modifier in modifiers:
            result.append({
                "id": modifier.id,
                "name": modifier.name,
                "type_id": modifier.type_id,
                "price": modifier.price,
                "max_count": modifier.max_count,
                "default": modifier.default,
                "image": modifier.image
            })
        return JSONResponse(content={"result": result}, status_code=status.HTTP_200_OK)
    except:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка создания типа модификации"
        )


@router.post("/modifier_type", summary="Создание типа модификации")
async def create_single_modifier_type(modifier_type: ModifierTypeCreate,
                                      db: Session = Depends(get_db)):
    try:
        modifier_type = create_modifier_type(modifier_type=modifier_type, db=db)
        logger.info(modifier_type)
        return JSONResponse(content={"name": modifier_type.name}, status_code=status.HTTP_201_CREATED)
    except:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка создания типа модификации"
        )


@router.get("/modifier_type", summary="Получение списка типов модификаций")
async def get_modifier_types(db: Session = Depends(get_db)):
    try:
        modifier_types = get_all_modifier_type(db=db)
        result = list()
        for mod_type in modifier_types:
            result.append({
                "id": mod_type.id,
                "name": mod_type.name
            })
        return JSONResponse(content={"result": result}, status_code=status.HTTP_200_OK)
    except:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ошибка создания типа модификации"
        )