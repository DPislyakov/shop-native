from fastapi import APIRouter

from apis.routes import auth
from apis.routes import user
from apis.routes import product
from apis.routes import cart
from apis.routes import home
from apis.routes import modifiers
from apis.routes import categories


api_router = APIRouter()

api_router.include_router(auth.router, prefix="/auth", tags=["Auth"])
api_router.include_router(user.router, prefix="/user", tags=["User"])
api_router.include_router(home.router, prefix="/home", tags=["Home"])
api_router.include_router(product.router, prefix="/product", tags=["Product"])
api_router.include_router(modifiers.router, prefix="/modifiers", tags=["Modifiers"])
api_router.include_router(categories.router, prefix="/categories", tags=["Categories"])
api_router.include_router(cart.router, prefix="/cart", tags=["Cart"])
