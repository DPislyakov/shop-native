from datetime import datetime, timedelta
from jose import jwt

from passlib.context import CryptContext
from typing import Union, Any, List
from enum import Enum

from core.config import settings
from db.models.product import Product

password_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def get_hashed_password(password: str) -> str:
    return password_context.hash(password)


def verify_password(password: str, hashed_pass: str) -> bool:
    return password_context.verify(password, hashed_pass)


def create_access_token(subject: Union[str, Any], expires_delta: int = None) -> str:
    if expires_delta is not None:
        expires_delta = datetime.utcnow() + expires_delta
    else:
        expires_delta = datetime.utcnow() + timedelta(minutes=30)

    to_encode = {"exp": expires_delta, "sub": str(subject)}
    encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, settings.ALGORITHM)
    return encoded_jwt


def create_refresh_token(subject: Union[str, Any], expires_delta: int = None) -> str:
    if expires_delta is not None:
        expires_delta = datetime.utcnow() + expires_delta
    else:
        expires_delta = datetime.utcnow() + timedelta(minutes=15)

    to_encode = {"exp": expires_delta, "sub": str(subject)}
    encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, settings.ALGORITHM)
    return encoded_jwt


def sort_enum(sort_list: List[str], sort_enum: Enum):
    res = []
    if len(sort_list) == 0:
        return res
    else:
        print(f"List {sort_list}")
        for sort_value in sort_list:
            print(sort_value)
            if sort_enum[sort_value].value == 1:
                res.append(Product.price)
            if sort_enum[sort_value].value == 2:
                res.append(Product.price.desc())
            if sort_enum[sort_value].value == 3:
                res.append(Product.name)
            if sort_enum[sort_value].value == 4:
                res.append(Product.name.desc())
            if sort_enum[sort_value].value == 5:
                res.append(Product.date)
            if sort_enum[sort_value].value == 6:
                res.append(Product.date.desc())
        return res
