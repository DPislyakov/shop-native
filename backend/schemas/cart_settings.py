from pydantic import BaseModel
from enum import Enum


class ValidateBaseModel(BaseModel):
    class Config:
        arbitrary_types_allowed = True


class CartOrder(ValidateBaseModel):
    value: int
    is_enabled: bool


class PaymentType(str, Enum):
    ONLINE: str = "online"
    OFFLINE: str = "offline"


class PaymentVariants(ValidateBaseModel):
    payment_type: PaymentType
    title: str


class OpeningHours(ValidateBaseModel):
    is_enabled: bool
    time_open: str
    time_close: str


class CartSettings(ValidateBaseModel):
    min_order_amount: CartOrder
    pay_variants: PaymentVariants
    opening_hours: OpeningHours
