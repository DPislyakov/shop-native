from pydantic import BaseModel

from typing import Optional


class ModifierCreate(BaseModel):
    name: str
    price: Optional[float]
    max_count: Optional[int]
    type_id: Optional[int]
    default: Optional[bool]
    image: Optional[str]
