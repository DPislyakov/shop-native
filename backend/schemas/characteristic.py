from pydantic import BaseModel


class CharacteristicCreate(BaseModel):
    title: str
    subtitle: str
    is_sub: bool = False
    product: int

