from pydantic import BaseModel

from typing import Optional


class CategoryCreate(BaseModel):
    title: str
    image: Optional[str]
    is_home: Optional[bool] = False

