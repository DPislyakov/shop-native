from pydantic import BaseModel

from typing import Optional, List


class PromoCreate(BaseModel):
    title: str
    description: Optional[str]
    image: Optional[str]
    category_id: Optional[int]
    url: Optional[str]
    type: Optional[str]


class PromoAdd(BaseModel):
    promo: int
