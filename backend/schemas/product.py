from pydantic import BaseModel, Json

from typing import Optional
from typing import List
from enum import Enum

from db.models.category import Category
from schemas.address import Address


class ValidateBaseModel(BaseModel):
    class Config:
        arbitrary_types_allowed = True


class ProductCreate(ValidateBaseModel):
    name: str
    description: Optional[str]
    price: Optional[float]
    type: Optional[str]
    unit: Optional[str]
    old_price: Optional[float]
    quantity: Optional[int]
    weight: Optional[int]
    length: Optional[int]
    width: Optional[int]
    height: Optional[int]
    categories: List[int] = []
    images: List[str] = []
    groups: List[int] = []
    is_favorite: Optional[bool] = False
    promos: Optional[List[int]]
    modifiers: List[int] = []


class ProductPreview(ValidateBaseModel):
    id: int
    name: str
    images: List[str]
    description: Optional[str]
    price: Optional[float]
    old_price: Optional[float]
    is_favorite: Optional[bool]


class ProductCart(ValidateBaseModel):
    amount: int
    product: ProductPreview


class ProductAddToCart(ValidateBaseModel):
    product_id: int
    amount: int


class CartOut(ValidateBaseModel):
    product: ProductPreview
    amount: int


class PaymentType(str, Enum):
    OFFLINE: str = "offline"
    ONLINE: str = "online"


class Payment(ValidateBaseModel):
    type: PaymentType
    num: str


class CartDelivery(ValidateBaseModel):
    id: int
    point: Optional[int]
    address: Optional[Address]
    comment: Optional[str]
    payment: Payment


class PaymentStatus(str, Enum):
    SUSPENDED: str = "suspended"
    PAID: str = "paid"


class CartOfferIn(ValidateBaseModel):
    delivery: CartDelivery
    payment_status: PaymentStatus
    payment_amount: float


class CartOffer(CartOfferIn):
    username: Optional[str]
    phone: str
