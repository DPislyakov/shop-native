from pydantic import BaseModel


class ModifierTypeCreate(BaseModel):
    name: str

