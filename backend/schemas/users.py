from pydantic import BaseModel
from pydantic import EmailStr
from datetime import date

from typing import Optional


class UserCreate(BaseModel):
    phone: str


class UserVerify(BaseModel):
    phone: str
    code: str


class ShowUser(BaseModel):
    name: Optional[str]
    phone: str
    email: Optional[EmailStr]
    sex: Optional[bool]
    birthday: Optional[date]

    class Config:
        orm_mode = True


class UpdateUser(BaseModel):
    name: Optional[str]
    email: Optional[EmailStr]
    sex: Optional[bool]
    birthday: Optional[str]

    class Config:
        orm_mode = True
