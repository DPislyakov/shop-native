from pydantic import BaseModel


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenVerifyIn(BaseModel):
    phone: str
    code: str


class TokenVerifyOut(BaseModel):
    access_token: str
    refresh_token: str


class TokenRefresh(BaseModel):
    refresh_token: str
