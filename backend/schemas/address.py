from pydantic import BaseModel

from typing import Optional


class ValidateBaseModel(BaseModel):
    class Config:
        arbitrary_types_allowed = True


class Address(ValidateBaseModel):
    city: Optional[str]
    street: Optional[str]
    house: Optional[str]
    entrance: Optional[str]
    floor: Optional[str]
    room: Optional[str]
    comment: Optional[str]
